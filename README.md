# TP1 EDBD : Bases de données relationnelles et UML
Groupe : 
Jules Cassan
Tom Gimat

Lien du google doc : https://docs.google.com/document/d/1zgFDRgnKOIBgH33Xu-RRJiYT4WmQOod9oERHhuI2c4I/edit?usp=sharing

IMPOTANT :: il faut  donner un nom pour la relation entre collections de photos et  photographie et passer les attributs ouverture focale etx en float
+ rajouter également une clef à configuration

## Cahier des charges #1 : les photos

+ Les utilisateurs sont reliés à l'entité _Photographie_

+ Les photographies sont prises par un unique appareil photo, qui doit posséder une certaine configuration au moment de la prise.
Pour cela nous avons choisi une entité ternaire _Configuration_ reliée à _Photographie_ et à _Appareil Photo_.
Nous avons choisi d'intégrer les informations de date de prise de la photo et du lieu dans l'entité _Photographie_

+ la licence sera représentée par une entité _Licence de Distribution_ qui sera associée à l'entité _Photographie_, chaque photo possède une licence et une licence peut être utilisée par une infinité de photos



## Cahier des charges #2 : publications, albums, et galeries


+ Ici, l'entité _Collection de photos_ représente un album ou une galerie de photos, nous avons donc 2 entités _Album_ et _Galerie_ qui hérite de _Collection de photos_.

+ _Collection de photos_ est associée à _Photographies_, chaque photo peut-être associée à aucune, une ou plusieurs collections de photos, et inversément chaque collection peut ne posséder aucune comme une infinité de photos.

+ Nous avons un lien entre _Utilisateur_ et _Collection de photos_ qui définit une création de collection par un utilisateur



## Cahier des charges #3 : interactions entre utilisateurs

+ Nous créons deux classes supplémentaires : "InteractionUtilisateur" et  "Marquage". Ces deux classes permettent d'enregistrer les différents types d'interactions entre les utilisateurs et sur les photographies. La première possède un attribu type qui nous permet de savoir si l'utilisateur a liké, commenté ou s'est abonné à un autre utilisateur. La seconde classe quant à elle, garde en mémoire les tags ou mots-clefs des photographies.

+ Pour prendre en compte le nombre de vues, il faudra implémenter une méthode afin d'incrémenter le nomnbre de vues d'une photographie lorsque celle-là sera vue.

2) Modèle physique des données : implémentation et requêtage sur Oracle

On a le choiix du schéma qu'on va implémenter. Soit on fait le second parce que c'est plus simple, soit on fait le troisième en partant du principe qu'il rapporte plus de points.

CREATE TABLE Utilisateur (
    IDutilisateur NUMBER(10) PRIMARY KEY
);

CREATE TABLE AppareilPhoto (
    IDappareil NUMBER(10) PRIMARY KEY
);

CREATE TABLE LicenceDistribution (
    IDlicence NUMBER(10) PRIMARY KEY,
    Type VARCHAR2(255)
);

CREATE TABLE Photographie (
    IDphoto NUMBER(10) PRIMARY KEY,
    Date DATE,
    Lieu FLOAT,
    IDutilisateur NUMBER(10) REFERENCES Utilisateur(IDutilisateur)
);

CREATE TABLE Configuration (
    IDconfiguration NUMBER(10) PRIMARY KEY,
    DistanceFocale FLOAT,
    OuvertureFocale FLOAT,
    TempsExposition FLOAT,
    Flash NUMBER(1)
);

-- Relation "publiée par" entre Utilisateur et Photographie
CREATE TABLE Publication (
    IDutilisateur NUMBER(10) REFERENCES Utilisateur(IDutilisateur),
    IDphoto NUMBER(10) REFERENCES Photographie(IDphoto),
    PRIMARY KEY (IDutilisateur, IDphoto)
);

-- Relation "possède" entre Photographie et LicenceDistribution
CREATE TABLE PossessionLicence (
    IDphoto NUMBER(10) REFERENCES Photographie(IDphoto),
    IDlicence NUMBER(10) REFERENCES LicenceDistribution(IDlicence),
    PRIMARY KEY (IDphoto, IDlicence)
);

-- Relation entre Photographie, Appareil Photo, et Configuration
CREATE TABLE PrisePar (
    IDphoto NUMBER(10) REFERENCES Photographie(IDphoto),
    IDappareil NUMBER(10) REFERENCES AppareilPhoto(IDappareil),
    IDconfiguration NUMBER(10) REFERENCES Configuration(IDconfiguration),
    PRIMARY KEY (IDphoto, IDappareil),
    FOREIGN KEY (IDphoto, IDconfiguration) REFERENCES Configuration(IDphoto, IDconfiguration)
);


-- Peupler la table Utilisateur
INSERT INTO Utilisateur (IDutilisateur) VALUES (1);
INSERT INTO Utilisateur (IDutilisateur) VALUES (2);
INSERT INTO Utilisateur (IDutilisateur) VALUES (3);
INSERT INTO Utilisateur (IDutilisateur) VALUES (4);
INSERT INTO Utilisateur (IDutilisateur) VALUES (5);

-- Peupler la table AppareilPhoto
INSERT INTO AppareilPhoto (IDappareil) VALUES (101);
INSERT INTO AppareilPhoto (IDappareil) VALUES (102);
INSERT INTO AppareilPhoto (IDappareil) VALUES (103);
INSERT INTO AppareilPhoto (IDappareil) VALUES (104);
INSERT INTO AppareilPhoto (IDappareil) VALUES (105);

-- Peupler la table LicenceDistribution
INSERT INTO LicenceDistribution (IDlicence, Type) VALUES (201, 'Tous droits réservés');
INSERT INTO LicenceDistribution (IDlicence, Type) VALUES (202, 'Utilisation commerciale autorisée');
INSERT INTO LicenceDistribution (IDlicence, Type) VALUES (203, 'Modifications autorisées');
INSERT INTO LicenceDistribution (IDlicence, Type) VALUES (204, 'Utilisation personnelle uniquement');
INSERT INTO LicenceDistribution (IDlicence, Type) VALUES (205, 'Licence Creative Commons');

-- Peupler la table Photographie
INSERT INTO Photographie (IDphoto, Date, Lieu, IDutilisateur) VALUES (301, TO_DATE('2023-09-15', 'YYYY-MM-DD'), 45.678, 1);
INSERT INTO Photographie (IDphoto, Date, Lieu, IDutilisateur) VALUES (302, TO_DATE('2023-09-16', 'YYYY-MM-DD'), 50.123, 2);
INSERT INTO Photographie (IDphoto, Date, Lieu, IDutilisateur) VALUES (303, TO_DATE('2023-09-17', 'YYYY-MM-DD'), 48.555, 3);
INSERT INTO Photographie (IDphoto, Date, Lieu, IDutilisateur) VALUES (304, TO_DATE('2023-09-18', 'YYYY-MM-DD'), 47.999, 4);
INSERT INTO Photographie (IDphoto, Date, Lieu, IDutilisateur) VALUES (305, TO_DATE('2023-09-19', 'YYYY-MM-DD'), 46.234, 5);

-- Peupler la table Configuration
INSERT INTO Configuration (IDconfiguration, DistanceFocale, OuvertureFocale, TempsExposition, Flash) VALUES (401, 24.0, 2.8, 1/250, 0);
INSERT INTO Configuration (IDconfiguration, DistanceFocale, OuvertureFocale, TempsExposition, Flash) VALUES (402, 35.0, 1.8, 1/500, 1);
INSERT INTO Configuration (IDconfiguration, DistanceFocale, OuvertureFocale, TempsExposition, Flash) VALUES (403, 50.0, 2.0, 1/1000, 0);
INSERT INTO Configuration (IDconfiguration, DistanceFocale, OuvertureFocale, TempsExposition, Flash) VALUES (404, 70.0, 2.8, 1/1250, 1);
INSERT INTO Configuration (IDconfiguration, DistanceFocale, OuvertureFocale, TempsExposition, Flash) VALUES (405, 85.0, 1.4, 1/2000, 0);

-- Peupler la relation "Publication" (utilisateur publie photographie)
INSERT INTO Publication (IDutilisateur, IDphoto) VALUES (1, 301);
INSERT INTO Publication (IDutilisateur, IDphoto) VALUES (2, 302);
INSERT INTO Publication (IDutilisateur, IDphoto) VALUES (3, 303);
INSERT INTO Publication (IDutilisateur, IDphoto) VALUES (4, 304);
INSERT INTO Publication (IDutilisateur, IDphoto) VALUES (5, 305);

-- Peupler la relation "PossessionLicence" (photographie possède licence)
INSERT INTO PossessionLicence (IDphoto, IDlicence) VALUES (301, 201);
INSERT INTO PossessionLicence (IDphoto, IDlicence) VALUES (302, 202);
INSERT INTO PossessionLicence (IDphoto, IDlicence) VALUES (303, 203);
INSERT INTO PossessionLicence (IDphoto, IDlicence) VALUES (304, 204);
INSERT INTO PossessionLicence (IDphoto, IDlicence) VALUES (305, 205);

-- Peupler la relation "PrisePar" (photographie prise par appareil photo avec configuration)
INSERT INTO PrisePar (IDphoto, IDappareil, IDconfiguration) VALUES (301, 101, 401);
INSERT INTO PrisePar (IDphoto, IDappareil, IDconfiguration) VALUES (302, 102, 402);
INSERT INTO PrisePar (IDphoto, IDappareil, IDconfiguration) VALUES (303, 103, 403);
INSERT INTO PrisePar (IDphoto, IDappareil, IDconfiguration) VALUES (304, 104, 404);
INSERT INTO PrisePar (IDphoto, IDappareil, IDconfiguration) VALUES (305, 105, 405);
